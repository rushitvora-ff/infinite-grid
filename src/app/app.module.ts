import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AppRoutingModule } from './app-routing.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { ProductDetailsComponent } from './dialog/product-details/product-details.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { AngularInfiniteCardGridModule } from 'angular-infinite-card-grid';

@NgModule({
  declarations: [
    AppComponent,
    ProductDetailsComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    AngularInfiniteCardGridModule,
    BrowserModule,
    NgxImageZoomModule,
    ModalModule.forRoot(),
    AppRoutingModule,
    CarouselModule,
    InfiniteScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
