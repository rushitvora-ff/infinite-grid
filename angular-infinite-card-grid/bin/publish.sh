#!/bin/bash

showHelp() {
cat << EOF
    Usage: ./publish --target-name name --target-version <prerelease|patch|minor|major> --target-distribution <dev|release> --target-repository repo --target-repository-url repoUrl --target-site-repository siteRepo --target-site-repository-url siteRepoUrl
    -h, -help,      --help          display help
    -V, -verbose,   --verbose       run script in verbose mode
EOF
}

CLEAN=0
available_target_versions=( "prerelease" "patch" "minor" "major" )
available_target_distributions=( "dev" "release" )

# https://stackoverflow.com/questions/16483119/an-example-of-how-to-use-getopts-in-bash
# $@ is all command line parameters passed to the script.
# -o is for short options like -v
# -l is for long options with double dash like --version
# the comma separates different long options
# -a is for long options with single dash like -version
options=$(getopt -l "help,verbose,target-name:,target-version:,target-distribution:,target-repository:,target-repository-url:,target-site-repository:,target-site-repository-url:" -o "hV" -a -- "$@")

# set --:
# If no arguments follow this option, then the positional parameters are unset. Otherwise, the positional parameters
# are set to the arguments, even if some of them begin with a ‘-’.
eval set -- "$options"

while true
do
    case $1 in
        -h|--help)
            showHelp
            exit 0
            ;;
        -V|--verbose)
            set -xv  # Set xtrace and verbose mode.
            ;;
        --clean)
            CLEAN=1
            ;;
        --target-name)
            shift
            target_name=$1
            ;;
        --target-version)
            shift
            for i in "${available_target_versions[@]}"
            do
                if [ "$1" == "$i" ]; then
                    target_version=( $1 )
                fi
            done
            ;;
        --target-distribution)
            shift
            for i in "${available_target_distributions[@]}"
            do
                if [ "$1" == "$i" ]; then
                    target_distribution=( $1 )
                fi
            done
            ;;
        --target-repository)
            shift
            target_repository=$1
            ;;
        --target-repository-url)
            shift
            target_repository_url=$1
            ;;

        --target-site-repository)
            shift
            target_site_repository=$1
            ;;
        --target-site-repository-url)
            shift
            target_site_repository_url=$1
            ;;
        --)
            shift
            break;;
    esac
    shift
done

if [ -z ${target_version+x} ]; then
    showHelp
    exit 0
fi

if [ -z ${target_distribution+x} ]; then
    showHelp
    exit 0
fi

if [ -z ${target_repository+x} ]; then
    showHelp
    exit 0
fi

if [ -z ${target_repository_url+x} ]; then
    showHelp
    exit 0
fi

if [ -z ${target_site_repository+x} ]; then
    showHelp
    exit 0
fi

if [ -z ${target_site_repository_url+x} ]; then
    showHelp
    exit 0
fi

echo "Target Name: $target_name"
echo "Target Version: $target_version"
echo "Target Distribution: $target_distribution"
echo "Target Repository: $target_repository"
echo "Target Repository Url: $target_repository_url"
echo "Target Site Repository: $target_site_repository"
echo "Target Site Repository Url: $target_site_repository_url"

# Directory of this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pushd . >> /dev/null
cd $DIR
cd ..

if [[ $(git status --porcelain) ]]; then
    echo "preparing to distribute, committing all changes..."
    set -x
    # As a pre-condition to distribute, we want to force users to add new files, so we will not do a git add --all on behalf of users
    git commit -a -m "preparing to distribute, committing all changes..."
    set +x
fi

if [[ -z $(git status --porcelain) ]]; then
    set -x

    npm version $target_version
    packageVersion=`jq -r .version package.json`

    if [[ $(git status --porcelain) ]]; then
        git commit -a -m "$packageVersion"
        git push
    fi

    # Publishing
    echo "Publishing code..."
    pushd . >> /dev/null
    workingDir=$(mktemp -d -t publish-$target_repository-XXXXXXXXXXXXXXXX)
    cd $workingDir
    git clone $target_repository_url

    cd $target_repository
    rm -rf *
    cp -r $DIR/../dist/$target_distribution/$target_name/* .
    rm -rf docs

    jq --arg packageVersion "$packageVersion" '.version = $packageVersion' package.json > package.json.tmp
    rm package.json
    mv package.json.tmp package.json

    git add --all
    git commit -a -m "$packageVersion"
    git push

    popd >> /dev/null
    rm -rf $workingDir

    # Distribute Site Docs
    echo 'Publishing site doc...'
    pushd . >> /dev/null
    workingDir=$(mktemp -d -t publish-$target_site_repository-XXXXXXXXXXXXXXXX)
    cd $workingDir
    git clone $target_site_repository_url

    cd $target_site_repository
    rm -rf $target_name/$target_distribution/$packageVersion
    mkdir -p $target_name/$target_distribution/$packageVersion
    cd $target_name/$target_distribution/$packageVersion
    cp -r $DIR/../dist/$target_distribution/$target_name/docs/* .
    git add --all
    git commit -a -m "$packageVersion"
    git push

    popd >> /dev/null
    rm -rf $workingDir

    # Add, commit, and push all files changed or added as part of the publishing process
    git add --all
    git commit -a -m "$packageVersion"
    git push

    set +x
else
    echo "This branch is not clean. We cannot publish if the branch is not clean."
    echo "npm run build then, as appropriate, git add, and git commit before attempting to publish."
fi

popd >> /dev/null