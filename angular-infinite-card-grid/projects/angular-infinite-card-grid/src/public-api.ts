/*
 * Public API Surface of angular-infinite-card-grid
 */

export * from './lib/angular-infinite-card-grid/angular-infinite-card-grid.component';
export * from './lib/angular-infinite-card-grid.module';
