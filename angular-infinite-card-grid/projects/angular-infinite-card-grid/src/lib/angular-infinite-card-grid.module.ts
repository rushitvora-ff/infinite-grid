import { NgModule } from '@angular/core';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { CommonModule } from '@angular/common';
import { ProductDetailsComponent } from './dialog/product-details/product-details.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { AngularInfiniteCardGridComponent } from './angular-infinite-card-grid/angular-infinite-card-grid.component';

@NgModule({
  declarations: [
    AngularInfiniteCardGridComponent,
    ProductDetailsComponent
  ],
  imports: [
    CommonModule,
    NgxImageZoomModule,
    ModalModule.forRoot(),
    CarouselModule,
    InfiniteScrollModule
  ],
  exports: [AngularInfiniteCardGridComponent]
})
export class AngularInfiniteCardGridModule { }
