import { AfterViewInit, Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit, AfterViewInit {
  showSlides = false;
  active = false;
  customOptions: OwlOptions = {
    loop: false,
    margin: 12,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 400,
    slideBy: 'page',
    navText: [
      '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
      '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
    ],
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
      },
      400: {
        items: 2,
      },
      500: {
        items: 3,
        stagePadding: 50,
      },
      700: {
        items: 3,
        stagePadding: 50,
      },
      768: {
        items: 4,
      },
      800: {
        items: 4,
      },
      820: {
        items: 4,
      },

      1200: {
        items: 5
      }
    },
    nav: true
  };
  details = `<p data-radium="true" style="overflow-wrap: break-word; font-size: 14px;">Farmer John Classic Premium Bacon is naturally hardwood smoked for full flavor. This pork bacon is a perfect addition to any dish and also perfect on its own as a filling snack. No matter if it's broiled  baked  or pan-fried  every meal is better with this hardwood smoked bacon. Whether wrapped around pork tenderloin bites  sprinkled on top of a baked apple with cinnamon and brown sugar  or added to your favorite burger or hot dog  these bacon strips are a great addition to your next meal or snack. Combine savory and sweet in a breakfast favorite by pouring pancake batter on these bacon strips while frying for delicious bacon pancake dippers. Wrap this tasty sliced bacon in aluminum foil before baking or broiling to keep it moist  cook more evenly  and to make clean up a breeze. Keep Farmer John Classic Bacon refrigerated.</p>`;

  myThumbnail = 'https://wittlock.github.io/ngx-image-zoom/assets/thumb.jpg';
  myFullresImage = 'https://wittlock.github.io/ngx-image-zoom/assets/fullres.jpg';
  showHeader = false;

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.showSlides = true;
    const content = document.querySelector('.modal');
    const scroll$ = fromEvent(content, 'scroll').pipe(
      map(() => content)
    );

    scroll$.subscribe(element => {
      if (element.scrollTop > 340) {
        this.showHeader = true;
      } else {
        this.showHeader = false;
      }
    });
  }

  count(length): any {
    return new Array(length);
  }

}
