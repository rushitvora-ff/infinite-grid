import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ProductDetailsComponent } from '../dialog/product-details/product-details.component';

@Component({
  selector: 'angular-infinite-card-grid',
  templateUrl: './angular-infinite-card-grid.component.html',
  styleUrls: ['./angular-infinite-card-grid.component.scss']
})
export class AngularInfiniteCardGridComponent implements OnInit {
  @Input() productList: any[];
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  filteredProduct = [];
  customOptions: OwlOptions = {
    loop: false,
    margin: 12,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 400,
    slideBy: 'page',
    navText: [
      '<i class="fa fa-chevron-left" aria-hidden="true"></i>',
      '<i class="fa fa-chevron-right" aria-hidden="true"></i>'
    ],
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
      },
      400: {
        items: 2,
      },
      500: {
        items: 3,
        stagePadding: 50,
      },
      700: {
        items: 3,
        stagePadding: 50,
      },
      768: {
        items: 4,
      },
      800: {
        items: 4,
      },
      820: {
        items: 4,
      },

      1200: {
        items: 5
      }
    },
    nav: true
  };
  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService) {
  }

  ngOnInit(): void {
    this.filteredProduct = this.productList.slice(0, 3);
  }

  onScrollDown(): void {
    setTimeout(() => {
      const from = this.filteredProduct.length;
      this.filteredProduct.push(...(this.productList.slice(from, from + 3) || []));
    }, 300);
  }

  count(length): any {
    return new Array(length);
  }

  openModalWithComponent(): void {
    const initialState = {};
    this.bsModalRef = this.modalService.show(ProductDetailsComponent, {class: 'modal-lg product-detail', initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }

}
